import React from 'react';
import FailedToLoadComponent from './FailedToLoadComponent';
class MicroFrontend extends React.Component {
  state = {error: false};
  componentDidMount() {
    
    const { name, host, document } = this.props;

    const scriptId = `micro-frontend-script-${name}`;

    if (document.getElementById(scriptId)) {
      this.renderMicroFrontend();
      return;
    }

    fetch(`${host}/asset-manifest.json`)
      .then(res => res.json())
      .then(manifest => {
        const script = document.createElement('script');
        script.id = scriptId;
        script.crossOrigin = '';
        script.src = `${host}${manifest['files']['main.js']}`;
        script.onload = this.renderMicroFrontend;
        document.head.appendChild(script);
      }).catch(err => {
        console.error("ooops");
this.setState({error : true});
      });
  }

  componentWillUnmount() {
    
    const { name, window } = this.props;

    window[`unmount${name}`] && window[`unmount${name}`](`${name}-container`);
  }

  renderMicroFrontend = () => {
    const { name, window, history } = this.props;
    
    window[`render${name}`] && window[`render${name}`](`${name}-container`, history);
  };

  render() {
    if(this.state?.error) return <FailedToLoadComponent/>;
    return <main id={`${this.props.name}-container`} />;
  }
}

MicroFrontend.defaultProps = {
  document,
  window,
};

export default MicroFrontend;